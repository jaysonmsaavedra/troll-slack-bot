const axios = require('axios')
const { RTMClient, WebClient } = require('@slack/client')
require('dotenv').config()

const rtm = new RTMClient(process.env.TOKEN)
const web = new WebClient(process.env.TOKEN)

rtm.start()

rtm.on('message', event => {
    if(event.text.startsWith('<@UC109VC74>')) {
        console.log(event.channel)
        axios.post(`https://slack.com/api/reactions.add?token=${process.env.TOKEN}&channel=${event.channel}&name=masterchief&timestamp=${event.ts}`)
        .then(res => console.log(res.data))
    }
})